const fetchUrl = window.location.protocol + '//' + window.location.host;
const selectWoj = document.getElementById('woj');
const selectPow = document.getElementById('pow');
const selectGmi = document.getElementById('gmi');
const selectMie = document.getElementById('mie');
const codes  = document.getElementById('codes');

function wait() {
    selectWoj.disabled = true;
    selectPow.disabled = true;
    selectGmi.disabled = true;
    selectMie.disabled = true;
    document.getElementById('wait').style.display = 'block';
}

function nowait() {
    document.getElementById('wait').style.display = 'none';
}

wait();
fetch(fetchUrl + '/xml').then(res => {
    return res.json();
}).then(data => {
    for(woj of data) {
        const option = document.createElement('option');
        option.value = woj;
        option.innerHTML = woj;
        selectWoj.appendChild(option);
        selectWoj.selectedIndex = -1;
        selectPow.selectedIndex = -1;
        selectGmi.selectedIndex = -1;
        selectMie.selectedIndex = -1;
    }

    selectWoj.disabled = undefined;
    nowait();
});

selectWoj.oninput = () => {
    wait();
    selectPow.innerHTML = '';
    selectGmi.innerHTML = '';
    selectMie.innerHTML = '';
    codes.innerHTML = '';
    fetch(fetchUrl + '/xml/' + selectWoj.value).then(res => {
        return res.json();
    }).then(data => {
        for(pow of data) {
            const option = document.createElement('option');
            option.value = pow;
            option.innerHTML = pow;
            selectPow.appendChild(option);
            selectPow.selectedIndex = -1;
            selectGmi.selectedIndex = -1;
            selectMie.selectedIndex = -1;
        }

        nowait();
        selectWoj.disabled = undefined;
        selectPow.disabled = undefined;
    });
}

selectPow.oninput = () => {
    wait();
    selectGmi.innerHTML = '';
    selectMie.innerHTML = '';
    codes.innerHTML = '';
    fetch(fetchUrl + '/xml/' + selectWoj.value + '/' + selectPow.value).then(res => {
        return res.json();
    }).then(data => {
        for(gmi of data) {
            const option = document.createElement('option');
            option.value = gmi.n;
            option.innerHTML = `${gmi.n} (${gmi.t})`;
            selectGmi.appendChild(option);
            selectGmi.selectedIndex = -1;
            selectMie.selectedIndex = -1;
        }

        nowait();
        selectWoj.disabled = undefined;
        selectPow.disabled = undefined;
        selectGmi.disabled = undefined;
    });
}

selectGmi.oninput = () => {
    wait();
    selectMie.innerHTML = '';
    codes.innerHTML = '';
    fetch(fetchUrl + '/xml/' + selectWoj.value + '/' + selectPow.value + '/' + selectGmi.value).then(res => {
        return res.json();
    }).then(data => {
        for(mie of data) {
            const option = document.createElement('option');
            option.value = mie;
            option.innerHTML = mie;
            selectMie.appendChild(option);
            selectMie.selectedIndex = -1;
        }

        nowait();
        selectPow.disabled = undefined;
        selectWoj.disabled = undefined;
        selectGmi.disabled = undefined;
        selectMie.disabled = undefined;
    });
}

selectMie.oninput = () => {
    wait();
    fetch(fetchUrl + '/xml/' + selectWoj.value + '/' + selectPow.value + '/' + selectGmi.value + '/' + selectMie.value).then(res => {
        return res.json();
    }).then(data => {
        let str = '<table><tr><th>Miasto</th><th>Ulica</th><th>Numery</th><th>Kod</th>';
        for(code of data) {
            str += `<tr><td>${code.city}</td><td>${code.street}</td><td>${code.nums}</td><td>${code.zip}</td></tr>`;
        }

        str += '</table>';
        codes.innerHTML = str;

        nowait();
        selectPow.disabled = undefined;
        selectWoj.disabled = undefined;
        selectGmi.disabled = undefined;
        selectMie.disabled = undefined;
    });
}
