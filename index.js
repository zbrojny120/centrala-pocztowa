const express = require('express');
const libxml = require('libxmljs');
const fs = require('fs');
const http = require('http');
const qs = require('querystring');

const app = express();
const port = process.env.PORT || 3000;

app.use('/static', express.static(__dirname + '/html'))

let terc, simc;

fs.readFile('terc.xml', 'utf8', (err, data) => {
    terc = libxml.parseXml(data);
});

fs.readFile('simc.xml', 'utf8', (err, data) => {
    simc = libxml.parseXmlString(data);
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/html/index.html');
});

app.get('/xml', (req, res) => {
    const wojs = terc.find('//row[NAZWA_DOD = "województwo"]/NAZWA').map(x => x.text());
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(wojs));
});

app.get('/xml/:woj', (req, res) => {
    const woj = terc.find(`//row[NAZWA_DOD = "województwo" and NAZWA = "${req.params.woj}"]/WOJ`).map(x => x.text())[0];
    const pows = terc.find(`//row[GMI = "" and WOJ = "${woj}" and NAZWA_DOD != "województwo"]/NAZWA`).map(x => x.text());
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(pows));
});

app.get('/xml/:woj/:pow', (req, res) => {
    const woj = terc.find(`//row[NAZWA_DOD = "województwo" and NAZWA = "${req.params.woj}"]/WOJ`).map(x => x.text())[0];
    const pow = terc.find(`//row[GMI = "" and WOJ = "${woj}" and NAZWA_DOD != "województwo" and NAZWA = "${req.params.pow}"]/POW`).map(x => x.text())[0];
    let gmis = terc.find(`//row[GMI != "" and WOJ = "${woj}" and POW = "${pow}"]/NAZWA`).map(x => x.text());
    let ts = terc.find(`//row[GMI != "" and WOJ = "${woj}" and POW = "${pow}"]/NAZWA_DOD`).map(x => x.text());
    gmis = gmis.filter((elem, index, self) => {
        return index === self.indexOf(elem);
    });

    const cs = [];
    for(let i in gmis) {
        cs.push({ n: gmis[i], t: ts[i]});
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(cs));
});

app.get('/xml/:woj/:pow/:gmi', (req, res) => {
    const woj = terc.find(`//row[NAZWA_DOD = "województwo" and NAZWA = "${req.params.woj}"]/WOJ`).map(x => x.text())[0];
    const pow = terc.find(`//row[GMI = "" and WOJ = "${woj}" and NAZWA_DOD != "województwo" and NAZWA = "${req.params.pow}"]/POW`).map(x => x.text())[0];
    const gmi = terc.find(`//row[GMI != "" and WOJ = "${woj}" and POW = "${pow}" and NAZWA = "${req.params.gmi}"]/GMI`).map(x => x.text())[0];
    const mies = simc.find(`//row[WOJ = "${woj}" and POW = "${pow}" and GMI = "${gmi}"]/NAZWA`).map(x => x.text());
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(mies));
});

app.get('/xml/:woj/:pow/:gmi/:mie', (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    const postData = qs.stringify({
        city: req.params.mie
    });

    const options = {
        hostname: 'pocztowekody.pl',
        port: 80,
        path: `/ajax/city`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postData)
        }
    };

    const codes_req = http.request(options, (codes_res) => {
        let rawData = '';

        codes_res.on('data', (chunk) => {
            rawData += chunk;
        });

        codes_res.on('end', () => {
            const jco = JSON.parse(rawData);
            let city = null;
            let str = '';
            for(const c of jco) {
                str = `${req.params.mie};${req.params.gmi};${req.params.pow};${req.params.woj}`;
                if(c.MiastoFull.toLowerCase() == str.toLowerCase()) {
                    city = c.IdMiasta;
                    break;
                }
            }

            if(city == null || str == '') {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify(['Brak kodów dla wybranego miejsca!']));
                return;
            }

            const path = `/?city=${str}&id_city=${city}`;

            const options2 = {
                hostname: 'pocztowekody.pl',
                port: 80,
                path: encodeURI(path),
                method: 'get',
            };

            let codes = [];

            const codes_req2 = http.get(options2, (codes_res2) => {
                let rawData = '';

                codes_res2.on('data', (chunk) => {
                    rawData += chunk;
                });

                codes_res2.on('end', () => {
                    codes = rawData.match(/1 z \d+/g)[0];
                    let ps = parseInt(codes.slice(4));
                    let pages = 0;
                    let pag = [];
                    for(let i = 1; i <= ps; ++i) {
                        const path = `/index/index/id_city/${city}/city/${str}/page/${i}`;
                        const options = {
                            hostname: 'pocztowekody.pl',
                            port: 80,
                            path: encodeURI(path),
                            method: 'GET',
                        };

                        const codes_req = http.get(options, (codes_res) => {
                            let rawData = '';

                            codes_res.on('data', (chunk) => {
                                rawData += chunk;
                            });

                            codes_res.on('end', () => {
                                let proc = rawData.match(/<td>.+<\/td>/g);
                                pag = pag.concat(proc);
                                pages++;

                                if(pages == ps) {
                                    const codes = [];
                                    let cnum = -1;
                                    pag = [].concat.apply([], pag);
                                    for(let p of pag) {
                                        if(p.match(/zipcode/)) {
                                            codes.push({ zip: '', nums: '', street: '', city: '' });
                                            ++cnum;
                                            codes[cnum].zip = p.match(/>.+>(.+)<.+</)[1];
                                        }
                                        else if(p.match(/street/)) {
                                            codes[cnum].street = p.match(/>.+>(.+)<.+</)[1];
                                        }
                                        else if(p.match(/city/)) {
                                            codes[cnum].city = p.match(/>.+>(.+)<.+</)[1];
                                        }
                                        else {
                                            codes[cnum].nums = p.match(/>(.+)</)[1];
                                        }
                                    }

                                    res.setHeader('Content-Type', 'application/json');
                                    res.send(JSON.stringify(codes));
                                }
                            });
                        });
                    }
                });
            });
        });
    });

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });

    codes_req.write(postData);
    codes_req.end();
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});
